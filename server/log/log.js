'use strict';
let {config} = require('../config');
class Log
{
    static log(content)
    {
        if(config.isLogged)
            console.log(content);
    }
}

module.exports.Log = Log;