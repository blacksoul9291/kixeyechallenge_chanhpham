'use strict';

let test = require('tape');
let {config} = require('../config');
let {RedisDatabase} = require('../database');
let {User} = require('../user');

test('-Test Redis config:', function (t)
{
    let db = new RedisDatabase(config);
    
    let recheckConnection = () =>
    {
        t.ok(db.isConnected(), '-connect');

        let oldUCount = db.getUserCount();
        User.createUser(genRandomString(), db, function(response)
        {
            db.loadUserCount(function()
            {
                let newuserId = response.data.id;

                t.ok(db.getUserCount() == (oldUCount + 1)
                    , '-increase userCount');
                t.ok(newuserId == (oldUCount + 1)
                    , '-increase id');
                t.ok(response != null
                        && response.code == config.responseCode._200.code
                    , '-create new user');
            });
        });

        let testId = 1;
        let testName = 'dasfa';
        let testScore = 12;
        User.updateOrCreateUser(testId, testName, testScore, db, function(response)
        {
            t.ok(response != null
                && response.code == config.responseCode._200.code 
                && response.data.id == testId
                && response.data.name == testName
                && response.data.score == testScore
            , '-update user');
        });

        let testId2 = Math.floor(Math.random() * 100000);
        let testName2 = genRandomString();
        let testScore2 = 12;
        User.updateOrCreateUser(testId2, testName2, testScore2, db, function(response)
        {
            t.ok(response.code == config.responseCode._404.code
            , '-update none exist user');
        });

        t.end();
    };

    setTimeout(recheckConnection, 1000);
});

function genRandomString() 
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
}
