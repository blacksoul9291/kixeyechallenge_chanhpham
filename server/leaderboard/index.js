let 
{   
    LeaderBoardService,
    AddingUserService, 
    UpdatingUserService, 
    StreamingScoreService,
    StatisticalService
} = require('./leaderboard_services');
let {LeaderBoard} = require('./leaderboard');

module.exports = 
{ 
    LeaderBoardService: LeaderBoardService,
    AddingUserService: AddingUserService,
    UpdatingUserService: UpdatingUserService,
    LeaderBoard: LeaderBoard,
    StreamingScoreService: StreamingScoreService,
    StatisticalService: StatisticalService
};