'use strict';
let {User} = require('../user');
let {Log} = require('../log');

let {config} = require('../config'); 

class LeaderBoardService
{
    constructor(path, port, database)
    {
        this.path = path;
        this.port = port;
        this.database = database;
        this.onRequestCallback = (req, res) => {this.onRequest(req, res);};
        this.onListenCallback = (req, res) => {this.onListen();};
        this.express = require('express');
        this.bodyParser = require('body-parser')
        this.app = this.express();
    }

    start()
    {
        this.initService();
        this.listen();
    }

    listen()
    {
        this.app.listen(this.port, this.onListenCallback);
    }

    initService(){}
    onListen(){}
    onRequest(req, res){}
}

class AddingUserService extends LeaderBoardService
{
    initService()
    {
        this.app.use(this.bodyParser.json());
        this.app.use(this.bodyParser.urlencoded({
            extended: false
        })); 
        this.app.put(this.path, this.onRequestCallback);
    }

    onListen()
    {
        Log.log("AddingUserService listen on port: " + this.port);
    }

    onRequest(req, res)
    {
        if (!req.is('application/json'))  
        { 
            res.send(config.responseCode._400); 
            return; 
        } 

        let callback = (response) => 
        {
            res.send(response);
            callback = null;
        };
        let name = req.body.name;
        User.createUser(name, this.database, callback);
    }
}

class StatisticalService extends LeaderBoardService
{
    setUpdatingUserService(service)
    {
        this.updatingUserService = service;
    }

    initService()
    {
        this.app.use(this.bodyParser.json());
        this.app.use(this.bodyParser.urlencoded({
            extended: false
        })); 
        this.app.get(this.path, this.onRequestCallback);

        this.startDate = Date.now();
        this.totalUpdateCount = 0;
    }

    onListen()
    {
        Log.log("UpdatingUserService listen on port: " + this.port);
    }

    onRequest(req, res)
    {
        if(this.updatingUserService == null || this.startDate == null)
            return;
        let totalHours = this.getRunningServiceTime();
        let totalUpdateCount = this.updatingUserService.getTotalUpdateCount();
        let updateCountPerHour = (totalHours > 0 && totalUpdateCount > 0) ? totalUpdateCount / totalHours : 0;

        let response =
        {
            totalHours: totalHours,
            totalUpdateCount: totalUpdateCount,
            updateCountPerHour: updateCountPerHour
        };

        res.send(response);
    }

    getRunningServiceTime()
    {
        return Math.abs(Date.now() - this.startDate) / 36e5;
    }
}

class UpdatingUserService extends LeaderBoardService
{
    setStremingScoreService(streamingScoreService)
    {
        this.streamingScoreService = streamingScoreService;
    }

    getTotalUpdateCount()
    {
        return this.totalUpdateCount;
    }

    initService()
    {
        this.app.use(this.bodyParser.json());
        this.app.use(this.bodyParser.urlencoded({
            extended: false
        })); 
        this.app.post(this.path, this.onRequestCallback);
        this.totalUpdateCount = 0;
    }

    onListen()
    {
        Log.log("UpdatingUserService listen on port: " + this.port);
    }

    onRequest(req, res)
    {
        if (!req.is('application/json'))  
        { 
            res.send(config.responseCode._400); 
            return; 
        }

        let id = req.body.id;
        let name = req.body.name;
        let score = req.body.score;

        let callback = (response) => 
        {
            res.send(response);
            if(!response.error && response.data != null && this.streamingScoreService != null)
            {
                this.streamingScoreService.send(response.data);
                this.totalUpdateCount++;
            }

            callback = null;
        };

        User.updateOrCreateUser(id, name, score, this.database, callback);
    }
}

class StreamingScoreService
{
    constructor(path, port)
    {
        this.path = path;
        this.port = port;

        this.WebSocket = require('ws');
    }

    start()
    {
        this.wss = new this.WebSocket.Server({ port: this.port, path: this.path });
        // this.wss.on('connection', function connection(ws) 
        // {

        // });
    }

    send(data)
    {
        if(this.wss == null)
        {
            Log.log("Wss is null");
            return;
        }

        if(data == null)
        {
            Log.log("data is null");
            return;
        }

        for(var client of this.wss.clients)
        {
            client.send(JSON.stringify(data));
        }
    }
}

class DeletingUserService extends LeaderBoardService
{
    initService()
    {
        this.app.use(this.bodyParser.json());
        this.app.use(this.bodyParser.urlencoded({
            extended: false
        })); 
        this.app.delete(this.path, this.onRequestCallback);
    }

    onListen()
    {
        Log.log("DeletingUserService listen on port: " + this.port);
    }

    onRequest(req, res)
    {
        // if (!req.is('application/json'))  
        // { 
        //     res.send(config.responseCode._400); 
        //     return; 
        // }

        let callback = (response) => 
        {
            res.send(response);
            callback = null;
        };

        let id = req.query.id;
        User.deleteUser(id, this.database, callback);
    }
}

class GettingUserService extends LeaderBoardService
{
    initService()
    {
        this.app.use(this.bodyParser.json());
        this.app.use(this.bodyParser.urlencoded({
            extended: false
        })); 
        this.app.get(this.path, this.onRequestCallback);
    }

    onListen()
    {
        Log.log("GettingUserService listen on port: " + this.port);
    }

    onRequest(req, res)
    {
        let callback = (response) => 
        {
            res.send(response);
            callback = null;
        };

        let id = req.query.id;
        User.getUser(id, this.database, callback);
    }
}

module.exports = 
{
    LeaderBoardService: LeaderBoardService, 
    AddingUserService: AddingUserService,
    UpdatingUserService: UpdatingUserService,
    DeletingUserService: DeletingUserService,
    GettingUserService: GettingUserService,
    StreamingScoreService: StreamingScoreService,
    StatisticalService: StatisticalService
};