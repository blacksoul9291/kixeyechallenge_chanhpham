'use strict';

let {LeaderBoardService
    , AddingUserService
    , UpdatingUserService
    , DeletingUserService
    , StreamingScoreService
    , GettingUserService
    , StatisticalService} = require('./leaderboard_services');

class LeaderBoard
{
    constructor(config, database)
    {
        this.config = config;
        this.database = database;
        this.streamingScoreService = new StreamingScoreService(config.streamingScoreService.path, config.streamingScoreService.port);        
        this.addingUserService = new AddingUserService(config.addingUserService.path, config.addingUserService.port, database);
        this.updatingUserService = new UpdatingUserService(config.updatingUserService.path, config.updatingUserService.port, database);
        this.deletingUserService = new DeletingUserService(config.deletingUserService.path, config.deletingUserService.port, database);
        this.getingUserService = new GettingUserService(config.getingUserService.path, config.getingUserService.port, database);
        this.statisticalService = new StatisticalService(config.statisticalService.path, config.statisticalService.port, database);
    }

    start()
    {
        this.streamingScoreService.start();
        this.addingUserService.start();
        this.updatingUserService.setStremingScoreService(this.streamingScoreService);
        this.updatingUserService.start();
        this.deletingUserService.start();
        this.getingUserService.start();
        this.statisticalService.start();
        this.statisticalService.setUpdatingUserService(this.updatingUserService);
    }
}

module.exports.LeaderBoard = LeaderBoard;