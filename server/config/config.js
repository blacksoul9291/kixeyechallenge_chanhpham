'use strict';

module.exports =
{
    config:
    {
        addingUserService:
        {
            path: "/user",
            port: 3000
        },
        updatingUserService:
        {
            path: "/user",
            port: 6783
        },
        deletingUserService:
        {
            path: "/user",
            port: 6784
        },
        getingUserService:
        {
            path: "/user",
            port: 6785
        },
        streamingScoreService:
        {
            path: "/update",
            port: 6786
        },
        statisticalService:
        {
            path: "/stats",
            port: 7787
        },
        redis:
        {
            host: "127.0.0.1",
            port: 6379
        },
        //config message end to client.
        responseCode: 
        {
            _200: {code: 200, message: "OK"},
            _400: {code: 400, message: "BAD REQUEST"}, 
            _404: {code: 404, message: "NOT FOUND"},
            _505: {code: 505, message: "INTERNAL SERVER ERROR"}
        },
        // turn on/off log function.
        isLogged: false
    }
};
