'use strict';

let {Log} = require('../log');

//Handle user logics
class User
{
    constructor(id, name, database)
    {
        if(database == null)
        {
            Log.log("database is null");
            return;
        }

        this.name = name;
        this.id = id;
        this.score = 0;
        this.updateCount = 0;
        this.database = database;
    }

    increaseUpdateCount()
    {
        this.updateCount++;
    }

    setName(name)
    {
        if(name == '' || name == null)
            return;

        this.name = name;
    }

    setScore(score)
    {
        if((score == 0 || score == null) 
            && (this.score == 0 || score == null))
        {
            this.score = 0;
            return;
        }

        this.score = score;
    }

    static createUser(name, database, callback)
    {
        let user = new User(database.getUserCount() + 1, name, database);
        let createCallback = (res) => 
        {
            if(!res.error)
                database.increaseUserCount();

            if(callback != null)
                callback(res);

            callback = null;
            createCallback = null;
        };
        user.saveToDB(createCallback);
    }

    static updateOrCreateUser(id, name, score, database, callback)
    {
        let user = new User(id, name, database);
        let loadCallback = (loadRes) =>
        {
            if(loadRes.error)
            {
                callback(loadRes);
                callback = null;
                return;
            }

            let saveCallback = (saveRes)=>
            {
                callback(saveRes);

                saveCallback = null;
                callback = null;
            };

            user.increaseUpdateCount();
            user.setName(name);
            user.setScore(score);
            user.saveToDB(saveCallback);

            loadCallback = null;
        }

        user.loadFromDB(loadCallback);
    }

    static deleteUser(id, database, callback)
    {
        let user = new User(id, null, database);
        user.deleteFromDB(callback);
    }

    static getUser(id, database, callback)
    {
        let user = new User(id, null, database);
        user.loadFromDB(callback);
    }

    deleteFromDB(callback)
    {
        this.database.deleteUser(this, callback);
        callback = null;
    }

    loadFromDB(callback)
    {
        this.database.loadUser(this, callback);
        callback = null;
    }
    
    deleteFromDB(callback)
    {
        this.database.deleteUser(this, callback);
        callback = null;
    }

    saveToDB(callback)
    {
        this.database.saveUser(this, callback);
        callback = null;
    }
}

module.exports = 
{
    User : User
};
