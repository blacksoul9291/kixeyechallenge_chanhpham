'use strict';

let {config} = require('./config');
let {LeaderBoard} = require('./leaderboard');
let {RedisDatabase} = require('./database');

//Implement the new class to switch database platform (Mysql, MongoDB, SQLite,...).
let database = new RedisDatabase(config);
let leaderboard = new LeaderBoard(config, database);
leaderboard.start();