'use strict';
let redis = require('redis');
let {Log} = require('../log');

const userCountKey = 'uCount';
const userKey = 'u:';

class RedisDatabase
{
    constructor(config)
    {
        this.uCount = 0;
        this.config = config;
        this.connected = false;
        this.client = redis.createClient(config.redis.port, config.redis.host);

        this.onErrorCallback = (err) => {this.onError(err)};
        this.client.on('error', this.onErrorCallback);

        this.onConnectCallback = () => {this.onConnect()};
        this.client.on('connect', this.onConnectCallback);

        this.getUserCountCallback = (err, obj) => {this.onGetUserCount(err, obj);};
    }

    onGetUserCount(err, obj)
    {
        if(obj != null)
            this.uCount = parseInt(obj);
        Log.log(this.uCount);
    }

    onConnect()
    {
        Log.log('Connect to redis!');
        this.loadUserCount(this.getUserCountCallback);
        this.connected = true;
    }

    loadUserCount(callback)
    {
        this.client.get(userCountKey, callback);
    }

    isConnected()
    {
        return this.connected;
    }

    onError(err)
    {
        Log.log(err);
    }

    getUserCount()
    {
        return this.uCount;
    }

    increaseUserCount()
    {
        this.uCount++;
        this.client.set(userCountKey, this.uCount);
    }

    loadUser(user, callback)
    {
        let loadCallback = (err, obj) =>
        {
            let resMsg = null;

            if(err != null)
            {
                resMsg = this.cloneMessage_505();
            }
            else if(obj == null)
            {
                resMsg = this.cloneMessage_404();
            }
            else
            {
                user.name = obj.n;
                user.score = obj.s;
                user.updateCount = obj.c;
                
                resMsg = this.cloneMessage_200();
                resMsg.data = 
                {
                    id: user.id, 
                    name: obj.n, 
                    score: obj.s, 
                    updateCount: obj.c
                };
            }

            if(callback != null)
                callback(resMsg);

            callback = null;
            loadCallback = null;
        };
        this.client.hgetall(userKey + user.id, loadCallback);
    }

    deleteUser(user, callback)
    {
        let delCallback = (err, res) =>
        {
            let resMsg = null;

            if(res == null || parseInt(res) <= 0 || err != null)
                resMsg = this.cloneMessage_505();
            else
                resMsg = this.cloneMessage_200();
            
            callback(resMsg);

            callback = null;
            delCallback = null;
        };
        this.client.del(userKey + user.id, delCallback);
    }

    saveUser(user, callback)
    {
        let responseCallback = (err, res) =>
        {
            if(callback == null) return;

            if(res != 'OK' || err != null)
            {
                Log.log(err);
                let resMsg = this.cloneMessage_505();
                callback(resMsg);

                return;
            }

            let resMsg = this.cloneMessage_200();
            resMsg.data = 
            {
                id: user.id, 
                name: user.name, 
                score: user.score, 
                updateCount: user.updateCount
            }
            callback(resMsg);

            callback = null;
            responseCallback = null;
        };
        this.client.hmset(userKey + user.id, {id: user.id, n: user.name, s: user.score, c: user.updateCount}, responseCallback);
    }

    cloneMessage_404()
    {
        let resMsg =         
        {
            code: this.config.responseCode._404.code,
            message: this.config.responseCode._404.message,
            error: true
        };

        return resMsg;
    }

    cloneMessage_505()
    {
        let resMsg =         
        {
            code: this.config.responseCode._505.code,
            message: this.config.responseCode._505.message,
            error: true
        };

        return resMsg;
    }

    cloneMessage_200()
    {
        let resMsg = 
        {
            code: this.config.responseCode._200.code,
            message: this.config.responseCode._200.message,
            error: false
        };

        return resMsg;
    }
}

module.exports.RedisDatabase = RedisDatabase;