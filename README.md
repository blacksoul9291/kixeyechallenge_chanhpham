# KIXEYEChallenge_ChanhPham

INSTRUCTION:

You can view the instruction on Youtube: https://youtu.be/PCS0rWKOmRw

## Installing:

- Download and install Nodejs: https://nodejs.org/en/
- Download and install Redis.
	For windows: https://github.com/MicrosoftArchive/redis/releases
- Open Command Prompt and install modules for Nodejs: 'redis', 'express', 'body-parser', 'ws', 'tape'.
	For example: npm install -s redis
	
## Running:

- Run redis.
	For windows: Extract and run redis-server.exe
- Open config.js in "[Your project folder]/server/config/" and change your configuration.
- Open Command Prompt in "[Your project folder]/server and command": "node app.js" to run Nodejs application.
- Open Command Prompt in "[Your project folder]/server/test and command": "node test.js" to run the unit test.

## REST APIs:

- Create a user (PUT method): Send username to create a new user.
	For example: 
	+ Send PUT method with body content: {"name": "John"} to http://127.0.0.1:3000/user (Default configuration).
	+ Response: 
	
		{
		"code": 200,
		"message": "OK",
		"error": false,
		"data": {
			"id": 1,
			"name": "John",
			"score": 0,
			"updateCount": 0
		}
}

	
- Update user info (POST method): Send new user info with valid id to update the user.
	For example: 
	+ Send POST method with body content: {"id":1,"name":"James", "score":10} to http://127.0.0.1:6783/user (Default configuration).
	+ Response:
	
		{
		"code": 200,
		"message": "OK",
		"error": false,
		"data": {
			"id": 1,
			"name": "James",
			"score": 10,
			"updateCount": 1
		}
}

	
- Get user (GET method): Get user full info with valid id.
	For example: 
	+ Send GET method to http://127.0.0.1:6785/user?id=1 (Default configuration).
	+ Response:
	
		{
		"code": 200,
		"message": "OK",
		"error": false,
		"data": {
			"id": 1,
			"name": "James",
			"score": 10,
			"updateCount": 1
		}
}

	
- Delete user (DELETE method): Delete user with valid id.
	For example: 
	+ Send DELETE method to http://127.0.0.1:6784/user?id=1 (Default configuration).
	+ Response: 
	
		{
		"code": 200,
		"message": "OK",
		"error": false
		}
		
	
- Get services statistics (GET method): Get current services statistics.
	For example: 
	+ Send GET method to http://127.0.0.1:7787/stats (Default configuration).
	+ Response: 
	
		{
		"totalHours": 0.01197,
		"totalUpdateCount": 15,
		"updateCountPerHour": 1253.1328320802006
		}

## Websocket:

- Update new score of another user via URL: ws://127.0.0.1:6786/update (Default configuration). You will be received new user info when someone updates their info.
	For example: 
	
		{
		"id": 3,
		"name": "John",
		"score": 10,
		"updateCount": 1
		}